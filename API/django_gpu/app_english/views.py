from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
import pandas as pd
import sys
sys.path.append('../../CODE/')
from api_call import main_call,detect_sentiment,detect_emotion,detect_lang,detect_entities,detect_emoticon

@api_view(['GET','POST'])
def get_all_analysis(request):
	if request.method == 'POST':
		# print(json.loads(request.data['df']))
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		flags = json.loads(request.data['flags'])
		print(flags)
		# print(df,col)
		result = main_call(df,col,flags)
		return Response(json.dumps(result.to_dict(orient='records')))



@api_view(['GET','POST'])
def get_sentiments(request):
	if request.method == 'POST':
		print('in sentiments')
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		result = detect_sentiment(df,col)
		return Response(json.dumps(result.to_dict(orient='records')))



@api_view(['GET','POST'])
def get_emotions(request):
	if request.method == 'POST':
		print('in emotions')
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		result = detect_emotion(df,col)
		return Response(json.dumps(result.to_dict(orient='records')))


@api_view(['GET','POST'])
def get_entities(request):
	if request.method == 'POST':
		print('in entities')
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		result = detect_entities(df,col)
		return Response(json.dumps(result.to_dict(orient='records')))


@api_view(['GET','POST'])
def get_language(request):
	if request.method == 'POST':
		print('in language')
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		result = detect_lang(df,col)
		return Response(json.dumps(result.to_dict(orient='records')))



@api_view(['GET','POST'])
def get_emoticons(request):
	if request.method == 'POST':
		print('in emoticons')
		df = pd.DataFrame(json.loads(request.data['df']))
		col = request.data['col']
		result = detect_emoticon(df,col)
		return Response(json.dumps(result.to_dict(orient='records')))


