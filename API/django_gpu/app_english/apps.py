from django.apps import AppConfig


class AppSpanishConfig(AppConfig):
    name = 'app_spanish'
