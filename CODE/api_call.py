import ast
import nltk
import emoji
import regex
import pandas as pd
from langdetect import detect as l_detect
from langdetect import detect_langs as lang_probs
from nltk import word_tokenize
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))


from INFERENCE.SENTIMENTS.inference import inference_sentiment
from INFERENCE.EMOTIONS.inference import inference_emotion
from INFERENCE.ENTITIES.inference import all_entities
from INFERENCE.EMOTICONS.inference import emoji_count
# from MUNGE.munge import munge


def detect_emoticon(df,col,flags={'emoticon':1}):
	if flags['emoticon'] != 1:
		df['emoticon'] = ['nan']*df.shape[0]
		return df

	df = df.apply(lambda row:emoji_count(row,col),axis=1)

	# print('emoji_count :',str(counter))
	return df

def detect_sentiment(df,col,flags={'sentiment':1}):
	# print(row['comment'])
	if flags['sentiment'] != 1:
		df['sentiment_score'] = ['nan']*df.shape[0]
		#When sentiment flag is 0, we keep confidence to 0.1 so its not considered
		df['sentiment_score_confidence'] = [0.1]*df.shape[0]
		return df

	df = inference_sentiment(df,col)
	return df

def detect_entities(df,col,flags={'entities':1}):
	if flags['entities'] != 1:
		df['entities'] = ['nan']*df.shape[0]
		return df

	df['entities'] = df[col].apply(all_entities)
	return df

def detect_lang(df,col,flags={'language':1}):
	if flags['language'] != 1:
		df['language'] = ['nan']*df.shape[0]
		return df

	df['language'] = df[col].apply(l_detect)
	return df

def detect_emotion(df,col,flags={'emotion':1}):
	if flags['emotion'] != 1:
		df['emotion'] = ['nan']*df.shape[0]
		df['emotions_score_condifence'] = ['nan']*df.shape[0]
		return df

	df = inference_emotion(df,col)
	df['emotion'] = df['emotion'].str.replace('Neutral','Flat')
	print(df['emotion'])
	return df

def main_call(df, col, flags):
	try:
		flags = ast.literal_eval(flags)
	except:
		pass
	# print(flags,'flags',type(flags))
	print('INPUT: '+str(df.shape[0])+' records for AI processing starts processing')
	if df.shape[0] == 0:
		return df

	# df = munge(df)
	df = detect_emoticon(df,col,flags)
	df = detect_entities(df,col,flags)
	df = detect_sentiment(df,col,flags)
	df = detect_lang(df,col,flags)
	df = detect_emotion(df,col,flags)
	print('OUTPUT: '+str(df.shape[0])+' records for AI processing finishes')

	return(df)


# if __name__ == '__main__':
# 	df = pd.read_csv('/home/azureuser/Documents/ML/CODE/FINE_TUNE_ENGLISH/EMOTIONS/OUTPUT/to_evaluate.csv').head(100)
# 	# print(df.columns)
# 	df = main_call(df,'text',{"sentiment": 1, "emotion": 1, "language": 1, "entities": 1, "emoticon": 1})
# 	df.to_csv('/home/azureuser/Documents/tbd/test_english.csv',index=False)
# 	exit()