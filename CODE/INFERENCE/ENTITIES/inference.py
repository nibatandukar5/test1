import spacy
import nltk
import sys
import pandas as pd
import os
import stanfordnlp
# stanfordnlp.download('en')
dir_path = os.path.dirname(os.path.realpath(__file__))

# sys.path.append(dir_path+'/RDRPOSTagger')
# from RDRPOSTag import RDRPOSTagger
# from RDRPOSTagger.Utility.Utils import getWordTag, getRawText, readDictionary


nlp = spacy.load("en_core_web_lg")
stan = stanfordnlp.Pipeline(lang='en',processors="tokenize,pos")

# rdrpos = RDRPOSTagger()
# rdrpos.constructSCRDRtreeFromRDRfile(dir_path+'/RDRPOSTagger/Models/MORPH/Spanish.RDR')
# DICT = readDictionary(dir_path+'/RDRPOSTagger/Models/MORPH/Spanish.DICT')

spacy_pos_tags = ['ADJ','ADV','INTJ','NOUN','NUM','PRON','PROPN']

nltk_pos_tags = ['JJ','JJR','JJS','NN','NNS','NNP','NNPS','PRP','PRP$','VB','VBD','VBG','VBN','VBP','VBZ']


def entities_from_spacy(text):
	doc = nlp(text)
	entities = []
	for token in doc:
		# print(token.text, token.pos_, token.dep_)
		if token.pos_ in spacy_pos_tags:
			entities.append(token.text)
	# print(entities)
	return entities

def entities_from_nltk(text):
	text = nltk.word_tokenize(text)
	pos_tagged = nltk.pos_tag(text)
	entities = [word[0] for word in pos_tagged if word[1] in nltk_pos_tags]

	return ','.join(entities)	


def entities_from_RDRPOSTagger(text):
	ents = rdrpos.tagRawSentence(DICT, text)
	# print(ents)
	return ents

def entities_from_stanfordnlp(text):
	entities = []
	# print('text',text)
	doc = stan(text)
	for sentence in doc.sentences:
		for word in sentence.words:
			if word.pos in spacy_pos_tags:
				entities.append(word.text)
	return entities


to_remove = ["á", "é", "í", "ñ", "ó", "ú", "ü", "¿", "¡", "Á", "É", "Í", "Ñ", "Ó", "Ú", "Ü", "«", "»", \
			"a", "de", "en", "por", "para", "que", "la", "el", "es", "y", "lo", "un", "una", "se", "con", "los", "las", "pero", \
			"yo", "eso", "su", "tu", "del", "al", "muy", "o", "él", "ella", "si", "sí", "no", "porque", "cuando", "donde", "aunque", \
			"sin", "con", "sobre", "mis", "sus", "te", "me", "siempre", "ahora",\
			"RT","0","1","2","3","4","5","6","7","8","9","e", "he", "ha", "nosotros", "vosotros",\
			"ellos", "ello","haber", "uy", "sobre", "entre","to","the","in","are","an","i","me","my","myself","we","our","ours","ourselves",\
			"you","your","yours","yourself","yourselves","he","him","his","himself","she","her","hers","herself","it","its","itself","they",\
			"them","their","theirs","themselves","what","which","who","whom","this","that","these","those","am","is","are","was","were","be",\
			"been","being","have","has","had","having","do","does","did","doing","a","an","the","and","but","if","or","because","as","until",\
			"while","of","at","by","for","with","about","against","between","into","through","during","before","after","above","below","to",\
			"from","up","down","in","out","on","off","over","under","again","further","then","once","here","there","when","where","why","how",\
			"all","any","both","each","few","more","most","other","some","such","no","nor","not","only","own","same","so","than","too","very",\
			"s","t","can","will","just","don","should","now"]
			
remove_entire = ["@","http://","https://","/","http",":"]
remove_only_symbol = ["#"]

# Special characters

# á, é, í, ñ, ó, ú, ü, ¿, ¡

# Á, É, Í, Ñ, Ó, Ú, Ü, «, »


# Frequently used prepositions/connectors/words

# a, de, en, por, para, que, la, el, es, y, lo, un, una, se, con, los, las, pero
# yo, eso, su, tu, del, al, muy, o, él, ella, si, sí, no, porque, cuando, donde, aunque
# sin, con, sobre, mis, sus, te, me, siempre, ahora,RT,0,1,2,3,4,5,6,7,8,9,e, he, ha, nosotros, vosotros
# ellos, ello,haber, uy, sobre, entre


def all_entities(text):
	words = text.split(' ')
	for sym in remove_entire:
		words = [_w for _w in words if sym not in _w]
	text = ' '.join(words)

	if not text:
		return ''

	ent1 = entities_from_spacy(text)
	# print(ent1)
	# ent2 = entities_from_nltk(text)
	# ent3 = entities_from_RDRPOSTagger(text)
	ent3 = entities_from_stanfordnlp(text)
	# return ','.join([ent1,ent2])
	total_ents = [_ent.lower() for _ent in (ent1+ent3) if _ent not in to_remove ]

	for sym in remove_entire:
		total_ents = [_ent for _ent in total_ents if sym not in _ent]

	for sym in remove_only_symbol:
		total_ents = [_ent.replace(sym,'') for _ent in total_ents]

	all_ents = ','.join(set(total_ents))
	return all_ents

