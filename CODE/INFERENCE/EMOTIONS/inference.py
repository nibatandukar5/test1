import re
from time import time
import pandas as pd
from transformers import pipeline
from transformers import BertTokenizer, BertForSequenceClassification
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
model_name = dir_path+"/model"
label_dict = {'excited':0,'happy':1,'neutral':2,'hopeful':3,'sad':4,'worried':5,'angry':6,'frustrated':7}

id_emotion = {v:k for k,v in label_dict.items()}

  
tokenizer_emotions = AutoTokenizer.from_pretrained(model_name)

model_emotions = AutoModelForSequenceClassification.from_pretrained(model_name,num_labels=8)


classifier_emotions = pipeline('sentiment-analysis', model=model_emotions, tokenizer=tokenizer_emotions,return_all_scores=True)
print('model loaded')

def inference_emotion(df,col):

	#Data Preprocessing
	def text_preprocessing(text):
		"""
		- Remove entity mentions (eg. '@united')
		- Correct errors (eg. '&amp;' to '&')
		@param    text (str): a string to be processed.
		@return   text (Str): the processed string.
		"""
		# Remove '@name'
		text = re.sub(r'(@.*?)[\s]', ' ', text)

		# Replace '&amp;' with '&'
		text = re.sub(r'&amp;', '&', text)

		# Remove trailing whitespace
		text = re.sub(r'\s+', ' ', text).strip()

		return text

	df['text_to_process'] = df[col].apply(text_preprocessing)
	# df = df.dropna()

	def predict(row):
		text = row['text_to_process']
		# print(text)

		predicted = classifier_emotions(text)[0]
		# print(predicted)
		for each_pred in predicted:
			emotion = id_emotion[int(each_pred['label'].replace('LABEL_',''))]
			row[emotion] = each_pred['score']

		max_score = max(predicted, key=lambda x:x['score'])

		row['emotion'] = id_emotion[int(max_score['label'].replace('LABEL_',''))]
		row['emotions_score_confidence'] = max_score['score']

		return row

	# a=time()
	# df = classifier_emotions(df['text_to_process'].values.tolist())
	df = df.apply(predict,axis=1).drop('text_to_process',axis=1)
	# print(time()-a)
	return df

# if __name__ == '__main__':
# 	df = pd.read_csv('/home/azureuser/Documents/ML/CODE/FINE_TUNE_ENGLISH/EMOTIONS/OUTPUT/to_test.csv').head(1000)
# 	df = inference_emotion(df,'text')
	# df.to_csv('/home/azureuser/Documents/MODELS/CODE/INFERENCE_ENGLISH/EMOTIONS/Output/bert_base.csv',index=False)

