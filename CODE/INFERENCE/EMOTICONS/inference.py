import regex
import emoji


def emoji_count(row,col):
	text = row[col]

	emoji_list = {}
	data = regex.findall(r'\X', text)
	# print(data)
	for word in data:
		# print(word)
		if any(char in emoji.UNICODE_EMOJI['es'] for char in word):
			# print(emoji.demojize(word),'caught')
			if emoji.demojize(word) in emoji_list.keys():
				emoji_list[emoji.demojize(word)][0] += 1
			else:
				emoji_list[emoji.demojize(word)] = [1,word]
				text = text.replace(word, ' ')
	# exit()
	row['emoticon'] = str(emoji_list)
	# print(str(emoji_list))
	row[col] = text
	return row



