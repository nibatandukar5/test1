import re
from time import time
import pandas as pd
from transformers import pipeline
from transformers import BertTokenizer, BertForSequenceClassification, BertConfig
import os


sentiment_dic = {'neutral':1,'positive':2,'negative':0}
id_sentiment = {v:k for k,v in sentiment_dic.items()}

dir_path = os.path.dirname(os.path.realpath(__file__))
model_name = dir_path+"/model"
# model_name = "/home/azureuser/Documents/MODELS/CODE/INFERENCE/SENTIMENTS/model"
# print(model_name)
from transformers import AutoTokenizer, AutoModelForSequenceClassification
  
tokenizer_sentiment = AutoTokenizer.from_pretrained(model_name)

model_sentiment = AutoModelForSequenceClassification.from_pretrained(model_name)

classifier_sentiment = pipeline('sentiment-analysis', model=model_sentiment, tokenizer=tokenizer_sentiment,return_all_scores=True)
print('model loaded')

def inference_sentiment(df,col):

	#Data Preprocessing
	def text_preprocessing(text):
		"""
		- Remove entity mentions (eg. '@united')
		- Correct errors (eg. '&amp;' to '&')
		@param    text (str): a string to be processed.
		@return   text (Str): the processed string.
		"""
		# Remove '@name'
		# print(text)
		text = re.sub(r'(@.*?)[\s]', ' ', text)

		# Replace '&amp;' with '&'
		text = re.sub(r'&amp;', '&', text)

		# Remove trailing whitespace
		text = re.sub(r'\s+', ' ', text).strip()

		return text

	df['text_to_process'] = df[col].apply(text_preprocessing)
	# df = df.dropna()

	def predict(row):
		text = row['text_to_process']
		# print(text)
		predicted = classifier_sentiment(text)[0]
		# print(predicted)
		for each_pred in predicted:
			sentiment = id_sentiment[int(each_pred['label'].replace('LABEL_',''))]
			row[sentiment] = each_pred['score']

		max_score = max(predicted, key=lambda x:x['score'])

		row['sentiment_score'] = id_sentiment[int(max_score['label'].replace('LABEL_',''))]
		row['sentiment_score_confidence'] = max_score['score']
		print(row)
		# exit()
		return row

	# a=time()
	# df = classifier_sentiment(df['text_to_process'].values.tolist())
	df = df.apply(predict,axis=1).drop('text_to_process',axis=1)
	# print(time()-a)
	return df

# if __name__ == '__main__':
# 	df = pd.read_csv('/home/azureuser/Documents/ML/CODE/FINE_TUNE_ENGLISH/SENTIMENTS/OUTPUT/to_test.csv').head(-1000)
# 	df = inference_sentiment(df,'text')
# 	df.to_csv('/home/azureuser/Documents/MODELS/CODE/INFERENCE_ENGLISH/SENTIMENTS/Output/cardiff_nlp_test2.csv')

